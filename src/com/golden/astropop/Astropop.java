package com.golden.astropop;

//Java
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

//GTGE
import com.golden.gamedev.*;
import com.golden.gamedev.object.*;
import com.golden.gamedev.object.background.*;
import com.golden.gamedev.object.font.SystemFont;
import com.golden.gamedev.util.ImageUtil;

//Astropop
import com.golden.astropop.sprites.*;
import com.golden.astropop.sprites.powerups.*;
import com.golden.astropop.spritegroups.*;

public class Astropop extends GameObject implements Comparator
{
	public AstropopGame game;
	public PlayField playfield;
	//public ParallaxBackground background;
	public Background background;
	
	public BufferedImage   SHIP_IMAGE, COLUMN_IMAGE, BARRIER_IMAGE, PROGRESS_BAR_IMAGE,
						   INFOBOARD;
	
	//Bricks					 
	public BufferedImage[] BRICK_RED, BRICK_GREEN, BRICK_BLUE, BRICK_YELLOW, BRICK_PURPLE, 
						   BRICK_STONE, BRICK_STEEL, BRICK_SKULL, BRICK_LINEBLAST, BRICK_RADIO,
						   BRICK_COLBLAST;
	
	//Powerup Animations
	public BufferedImage[] RADIO_BLAST, LINE_BLAST, COL_BLAST;
	
	public SpriteGroup BRICK_GROUP;
	
	public ColumnMarker column;
	public Player player;
	public ProgressBar progressBar;
	
	public GameFont font_40pt;
	
	//Loading Screen Variables
	private GameFont loadingFont;
	private BufferedImage ARROW;
	private Sprite arrow;
	private ColorBackground clrBackground;
	private Timer loadingTimer = new Timer(5000); //5 seconds
	
	//Game State Variables
	private int gameState;
	public static final int LOADING = 0;
	public static final int PLAYING = 1;
	public static final int WON 	= 2;
	public static final int LOST 	= 3;
	
	//Game Logic Variables
	private boolean movingLeft   = false;
	private boolean movingRight  = false;
	private boolean drawingCombo = false;
	private boolean bricksExploded;
	
	private int bricksExplodedSinceAdvance	= 0;       
	private int comboCount					= 0;
	private int finalComboVal				= 0;
	private int bricksNeededToWin;
	private final int advancementLimit 		= 12;
	private int[] lastColorsAdded			= new int[2];
	
	private double harmfulBrickProbability;
	private double powerupProbability;
	
	private Timer explosionDelay		= new Timer(500);
	private Timer advancementTimer;		//= new Timer(7000); //7000
	private Timer comboDisplayTimer		= new Timer(4000);
	
	private int level;
		
	//General Brick types
	public static final int COLORED		= 0;
	public static final int POWERUP		= 1;
	public static final int HARMFUL		= 2;		
			
	// Brick type variables
	public static final int COLORLESS  	= -1;	//Color types
	public static final int RED 	   	= 0;
	public static final int GREEN  	   	= 1;
	public static final int BLUE 	   	= 2;
	public static final int YELLOW 	   	= 3;
	public static final int PURPLE     	= 4;
	public static final int TURQUOISE  	= 5; 	
		
	public static final int RADIOBLAST 	= 6; 	//Power-ups
	public static final int LINEBLAST  	= 7; 	
	public static final int COLBLAST	= 8;	
	public static final int SUPERCHARGE	= 9;	
	public static final int BONUS		= 10;	
		
	public static final int STONE 	   	= 11;	//Harmful types
	public static final int STEEL	   	= 12;
	public static final int SKULL	   	= 13;
	
	//Direction variables
	public static final int LEFT = -1;
	public static final int RIGHT = 1; 
	
 /****************************************************************************/
 /******************************* CONSTRUCTOR ********************************/
 /****************************************************************************/
 	public Astropop(AstropopGame parent, int level)
 	{
 		super(parent);
 		game = parent;
 		this.level = level;
 		hideCursor();
 	}
 	
 /****************************************************************************/
 /************************* COMPARATOR IMPLEMENTATION ************************/
 /****************************************************************************/
 	//Sort bricks by row (lowest row takes precedence) then column (left to right)
 	public int compare(Object o1, Object o2)
 	{
 		AnimAstropopSprite s1 = (AnimAstropopSprite) o1;
 		AnimAstropopSprite s2 = (AnimAstropopSprite) o2;
 		if (s1.getLocY() != s2.getLocY())
 			return s2.getLocY() - s1.getLocY();
 			
 		return (s1.getLocX() - s2.getLocX());
 	}
 	
 /****************************************************************************/
 /**************************** INIT RESOURCES ********************************/
 /****************************************************************************/
 	public void initResources()
 	{
 		//Initiate Level Data
 		harmfulBrickProbability = .03 + .005 * level;
 		powerupProbability = .02;
 		advancementTimer = new Timer(10000 - 250* level);
 		bricksNeededToWin = 100 + 50 * level;
 		
 		
 		//Loading Screen Resources
 		clrBackground = new ColorBackground(Color.BLACK);
 		ARROW = getImage("Assets//Images//arrow.jpg");
 		arrow = new Sprite(ARROW, 350, 500);
 		loadingFont = fontManager.getFont(getImages("Assets//Images//font.png", 16, 6));
 		
 		//Load Font
		try
		{
			File file = new File("com//golden//astropop//Assets//Fonts//evanescent.ttf");
			FileInputStream fis = new FileInputStream(file);
			Font tempFont = Font.createFont(Font.TRUETYPE_FONT, fis);
			Font tempFont40 = new Font(tempFont.getFontName(), Font.PLAIN, 52);
			font_40pt = new SystemFont(tempFont40, new Color(234, 188, 29)); //177, 3, 3
		}
		catch (Exception e){System.out.println(e);}
 		
 		//Load Images
 		BRICK_RED 			= getImages("Assets//Images//Brick_Red.png",	   3,  7);
 		BRICK_GREEN			= getImages("Assets//Images//Brick_Green.png", 	   3,  7);
 		BRICK_BLUE 			= getImages("Assets//Images//Brick_Blue.png", 	   3,  7);	
 		BRICK_YELLOW 		= getImages("Assets//Images//Brick_Yellow.png",    3,  7);
 		BRICK_PURPLE		= getImages("Assets//Images//Brick_Purple.png",    3,  7);
 		BRICK_STONE			= getImages("Assets//Images//Brick_Stone.png", 	   3,  7);
 		BRICK_STEEL			= getImages("Assets//Images//Brick_Steel.png", 	   3,  7);
 		BRICK_SKULL			= getImages("Assets//Images//Brick_Skull.png", 	   3,  1);
 		BRICK_LINEBLAST		= getImages("Assets//Images//Brick_LineBlast.png", 3,  7);
 		BRICK_RADIO			= getImages("Assets//Images//Brick_Radio.png",	   3,  7);
 		BRICK_COLBLAST		= getImages("Assets//Images//Brick_ColBlast.png",  3,  7);
 		LINE_BLAST			= getImages("Assets//Images//LineBlast.png",	   1,  6);
 		RADIO_BLAST			= getImages("Assets//Images//RadioBlast.png", 	   4,  5);
 		//Non-animating sprite images
 		SHIP_IMAGE 	 		= getImage ("Assets//Images//Ship.png"			);
 		BARRIER_IMAGE		= getImage ("Assets//Images//Barrier.png"		);
 		PROGRESS_BAR_IMAGE	= getImage ("Assets//Images//ProgressBar.png"	);
 		//Sprite images that have special opacities
 		COLUMN_IMAGE = ImageUtil.getImage(bsIO.getURL("Assets//Images//Column.png"),    Transparency.TRANSLUCENT);
 		INFOBOARD    = ImageUtil.getImage(bsIO.getURL("Assets//Images//Infoboard.png"), Transparency.TRANSLUCENT);
 		COL_BLAST    = ImageUtil.getImages(bsIO.getURL("Assets//Images//ColBlast.png"), 7, 1, Transparency.TRANSLUCENT);

 		
 		//Background and PlayField
 		background = new ImageBackground( getImage("Assets//Images//background_2.jpg") );
 		//background2 = new ImageBackground( getImage("Assets//Images//Background_Stars.png") );
 		//background = new ParallaxBackground(new Background[] { background1 });
 		playfield = new PlayField(background);
 		playfield.add( new Sprite(INFOBOARD, 0, 0) );
 		
 		//Create Column Marker
 		column = new ColumnMarker(COLUMN_IMAGE, new Location(4, 0));
 		playfield.add(column);
 		
 		//SpriteGroup for Bricks
 		BRICK_GROUP 	= playfield.addGroup( new BrickGroup("Brick Group", this) ); 
 		
 		//Create Bricks
 		fillBoardWithBricks();
 		/*for (int y = 0; y < 6; y++)
 			addRow();*/
 			
 		/*Assign special brick probabilities
 		harmfulBrickProbability = .08; //.08
 		powerupProbability = .05; //.05 */
 			
 		//Create Player
 		player = new Player(SHIP_IMAGE, new Location(4, 22));
 		playfield.add(player);
 		
 		//Create Brick Barrier (Line that separates upcoming bricks from the rest of the bricks)
 		playfield.add( new Sprite(BARRIER_IMAGE, 200, 25) );
 		
 		//Create Progress Bar
 		progressBar = new ProgressBar(PROGRESS_BAR_IMAGE, 700, 0, 30, 500, bricksNeededToWin);
 		playfield.add(progressBar);
 		
 		gameState = PLAYING;
 	}
 	
 /****************************************************************************/
 /***************************** UPDATE GAME **********************************/
 /****************************************************************************/
 	public void update(long elapsedTime)
 	{
 		switch (gameState)
 		{
 			case LOADING:
 				loading(elapsedTime);
 				break;
 				
 			case PLAYING:
 				//checkInput(elapsedTime);
 				if (Brick.bricksHaveFallen()) //if some bricks have recently finished falling
 					checkExplode();         //check for four touching bricks of same color
 				if (Brick.bricksHaveExploded())
 					checkFall();
 				if (Brick.getConvertedBricks().size() > 0)
 					convertBricks();
 				if (comboCount == 0)
 				{
 					checkWin();
 					if (bricksExplodedSinceAdvance > advancementLimit || advancementTimer.action(elapsedTime))
 						addRow();
 				}
 				updateComboDisplay(elapsedTime);
 				playfield.update(elapsedTime);
 				checkInput(elapsedTime);
 				break;
 				
 			case WON:
 				parent.nextGameID = AstropopGame.ASTROPOP;
 				finish();
 				break;
 				
 			case LOST:
 				break;
 		}
 	}
 	
 	public void addRow()
 	{
 		//if any bricks are in the process of iterating through their explosion animations...
 		if (Brick.bricksAreExploding() || PowerupAnimation.powerupsAnimating())
 		{
 			//...guarantee that addRow() will be called again on the next game loop iteration...
 			bricksExplodedSinceAdvance = advancementLimit + 1;
 			return;   //...but don't do anything else
 		}
 		
 		//check if adding a row would result in a collision with the player or the bottom of the game
 		if (checkLose())
 			return;
 			
 		//move every brick down one space
 		for (Brick brick : Brick.getAllBricks())
 		{
 			if (!brick.isPulled())
 			{ 
 				brick.moveY(1);
 				if (!brick.isThrown() && !brick.isFalling())
 					brick.setMovementState(Brick.ADVANCING);
 			}
 		}
 		
 		//add a new row
 		int color;
 		double randNum;
 		Arrays.fill(lastColorsAdded, -2); //essentially null value (0 and -1 were both taken)
 		for (int x = 0; x < 10; x++)
 		{
 			randNum = Math.random();
 			//add a harmful brick if the odds say to...
 			if (randNum < harmfulBrickProbability)
 			{
 				//if there's already skulls in play, don't add more
 				if (Brick.numSkullBricks > 0)
 				{
 					color = getRandom(STONE, STEEL);
 				}
 				else
 				{
 					color = getRandom(STONE, SKULL);
 					if (color == SKULL)
 						Brick.numSkullBricks++;
 				}
 			}	
 			//...or add a powerup if the odds say to...
 			else if (randNum >= harmfulBrickProbability 
 					 && randNum < harmfulBrickProbability + powerupProbability)
 				{
 					 color = getRandom(RADIOBLAST, COLBLAST);
 				}
 			//...otherwise add a regular colored brick
 			else
 			{
 				//if the last two bricks were the same color, pick a different color
 				do
 				{
 					color = getRandom(RED, PURPLE);
 				} 
 				while (lastColorsAdded[0] == color && lastColorsAdded[1] == color);
 			}
 			//refresh the list of last colors added
 			lastColorsAdded[1] = lastColorsAdded[0];
 			lastColorsAdded[0] = color;
 			//and add the brick to the game
 			playfield.add( new Brick( this, getBrickImage(color), new Location(x, 0), color ) );
 		}
 		
 		bricksExplodedSinceAdvance = 0;
 		advancementTimer.refresh();
 	}
 	
 	public void addHarmfulBrick(int brickType, Location loc)
 	{
 		Brick.removeBrick( Brick.getBrickAtLocation(loc) );
 		Brick brick = new Brick( this, getBrickImage(brickType), loc, brickType );
 		playfield.add(brick);
 	}
 	
 	public void checkExplode()
	{
		ArrayList<Brick> playingBricks = Brick.getPlayingBricks();
		ArrayList<Brick> bricksOfSameColor = new ArrayList<Brick>();
		bricksExploded = false;
		for (Brick tempPlayingBrick : playingBricks)
		{			
			//if brick has recently fell and is not already exploding
			if ( tempPlayingBrick.isExplodable() && !tempPlayingBrick.isExploding())
			{
				bricksOfSameColor.add(tempPlayingBrick);
				bricksOfSameColor = Brick.getAdjacentSameColorBricks(tempPlayingBrick, bricksOfSameColor);
				
				//if four or more are touching, blow em up, and find any nearby power-ups/HarmfulBricks
				if (bricksOfSameColor.size() > 3)
				{
					ArrayList<Brick> powerups = Brick.getAdjacentPowerups(bricksOfSameColor, 1);
					ArrayList<Brick> harmfulBricks = Brick.getAdjacentHarmfulBricks(bricksOfSameColor, 1);
					explodeBricks(powerups);
					explodeBricks(harmfulBricks);
					explodeBricks(bricksOfSameColor);
					comboCount++;
					bricksExploded = true;
					Brick.setBricksHaveFallen(false);
					//return; //playingBricks will be obsolete after exploded bricks are set inactive, so method returns
				}
				
				//reset the list of adjacent bricks of the same color
				bricksOfSameColor.clear();
			}
		}
		Brick.setBricksHaveFallen(false);
		if (bricksExploded == false)
			finishCombo();
	}
	
	public void checkFall()
	{
		Location tempLoc;
		int numOfGaps;
		boolean checkingForGaps;
		boolean bricksFell = false;
		Brick tempBrick;
		
		//for every column
		for (int x = 0; x < 10; x++)
		{
			checkingForGaps = false;
			//start checking at the bottom of the column...
			for (int y = 23; y > 0; y--)
			{
				tempLoc = Location.getLocationAt(x, y);
				if (tempLoc != null)  //if the location is occupied...
				{
					//...and is a brick...
					if (tempLoc.getType() == Location.BRICK_LOC)
						checkingForGaps = true; //...then start checking for gaps in the column
				}
				//otherwise if it's a gap, and there are bricks below it
				else if (checkingForGaps)
				{
					//move every brick above it up one space
					for (int tempY = y; tempY < 24; tempY++)
					{
						tempBrick = Brick.getBrickAtLocation(x, tempY);
						if (tempBrick != null && tempBrick.isPulled() == false)
						{
							tempBrick.moveY(-1);
							tempBrick.setMovementState(Brick.FALLING);
							bricksFell = true;
						}
					}
				}		
			}
		}
		Brick.setBricksHaveExploded(false);
		if (!bricksFell)
			finishCombo();
	}
		
	public void checkInput(long elapsedTime)
 	{	
 		//Debugging	
 		if ( keyPressed(KeyEvent.VK_Q) )
 			addRow();
 			
 		if ( keyPressed(KeyEvent.VK_W) )
 			lineBlast(2);
 			
 		if ( keyPressed(KeyEvent.VK_E) )
 			colBlast(player.getLocX());
 		
 		//Reset on ENTER
 		if ( keyPressed(KeyEvent.VK_ENTER) )
 		{
 			parent.nextGameID = AstropopGame.ASTROPOP;
 			finish();
 		}
 		
 		//Main menu on H
 		if ( keyPressed(KeyEvent.VK_H) )
 		{
 			parent.nextGameID = AstropopGame.MAIN_MENU;
 			finish();
 		}
 		
 		//Check leftward movement
 		if ( keyPressed(KeyEvent.VK_A) || keyPressed(KeyEvent.VK_LEFT) )
 		{		
 			movePlayer(LEFT);
 			player.moveTimer.refresh();	//start moving every .125 while button depressed
 			return;
 		}
 		
 		//Check rightward movement	
 		if ( keyPressed(KeyEvent.VK_D) || keyPressed(KeyEvent.VK_RIGHT) || keyPressed(KeyEvent.VK_S) )
 		{
 			movePlayer(RIGHT);
 			player.moveTimer.refresh();	//start moving every .125 while button depressed
 			return;
 		}
 		
 		//Continue moving every 125 miliseconds if arrow key depressed
		if ( player.moveTimer.action(elapsedTime) )
		{
 			if ( keyDown(KeyEvent.VK_A) || keyDown(KeyEvent.VK_LEFT) )
 				movePlayer(LEFT);
 			if ( keyDown(KeyEvent.VK_D) || keyDown(KeyEvent.VK_RIGHT))
 				movePlayer(RIGHT);
 		}
 		
 		if (comboCount == 0)
 		{
 			//Check for pulling and dumping bricks (if game engine fails at registering clicks, compensate)
 			try 
 			{
 				if (click() || keyPressed(KeyEvent.VK_UP) || keyPressed(KeyEvent.VK_K))
 					throwBricks();
 			}
 			catch (ArrayIndexOutOfBoundsException e)
 			{
 				throwBricks();	
 			}
 			try 
 			{
 				if (rightClick() || keyPressed(KeyEvent.VK_DOWN) || keyPressed(KeyEvent.VK_J))
 					pullBricks();
 			}
 			catch (ArrayIndexOutOfBoundsException e) 
 			{
 				pullBricks();
 			}
 		}
 	}
 	
 	public boolean checkLose() 
 	{
 		ArrayList<Brick> lowestBricks = new ArrayList<Brick>(10);
 		Brick tempLowestBrick;
 		int x, y;
 		
 		//for each column
 		Outer:
 		for (x = 0; x < 10; x++)
 		{
 			//for each row in that column, up to the 8th row from the bottom
 			for (y = 23; y >= 15; y--)
 			{
 				tempLowestBrick = Brick.getPlayingBrickAtLocation(x, y);
 				
 				//if there is a playing brick at the current position, add it to the array...
 				if (tempLowestBrick != null)
 				{
 					lowestBricks.add(tempLowestBrick);
 					continue Outer; //...then skip to the next column
 				}
 			}
 		}
 		
 		int lowestBrickX, lowestBrickY;
 		
 		//for each of the lowest bricks in play
 		for (Brick brick : lowestBricks)
 		{
 			lowestBrickX = brick.getLocX();
 			lowestBrickY = brick.getLocY();
 			
 			//if there is a brick or player beneath it, or its on the bottom row...
 			if ( Location.locationOccupied(lowestBrickX, lowestBrickY + 1) || lowestBrickY == 23)
 			{
 				gameState = LOST;  //...then the player loses
 				return true;	   //...and tell addRow() that there is no space for a new row
 			}
 		}
 		return false;
 	}
 	
 	public void checkWin()
 	{
 		if (progressBar.getPercent() == 1)
 			gameState = WON;
 	}
 	
 	public void colBlast(int column)
 	{
 		//add the actual image of the ColBlast explosion
 		playfield.add(new ColBlast(COL_BLAST, column));
 		
 		ArrayList<Brick> bricksInColumn = Brick.getBricksInColumn(column);
 		comboCount++;
 		explodeBricks(bricksInColumn, 2);
 		bricksExploded = true;
 	}
 	
 	public void convertBricks()
	{
		for (Brick brick : Brick.getConvertedBricks())
		{
			addHarmfulBrick(SKULL, brick.getLocation());
		}
		Brick.clearConvertedBricks();
	}
 
	public void explodeBricks(ArrayList<Brick> bricks)
	{
		//Blow up the bricks, and increment the number of bricks exploded
		for (Brick tempBrick : bricks)
		{
			if (tempBrick.isExploding() == false)
			{
				tempBrick.explode();
				bricksExplodedSinceAdvance++;
				progressBar.incrementValue();
			}
		}
	}
	
	public void explodeBricks(ArrayList<Brick> bricks, int damage)
	{
		//Blow up the bricks, and increment the number of bricks exploded
		for (Brick tempBrick : bricks)
		{
			if (tempBrick.isExploding() == false)
			{
				tempBrick.explode(damage);
				bricksExplodedSinceAdvance++;
				progressBar.incrementValue();
			}	
		}
	}
	
	public void fillBoardWithBricks()
	{
 		int color;
 		for (int y = 0; y < 6; y++)
 		{	
 			Arrays.fill(lastColorsAdded, -2); //essentially null value (0 and -1 were both taken)
 			for (int x = 0; x < 10; x++)
 			{
 				do
 				{
 					color = getRandom(RED, PURPLE);
 				} 
 				while (lastColorsAdded[0] == color && lastColorsAdded[1] == color);
 		    	//refresh the list of last colors added
 				lastColorsAdded[1] = lastColorsAdded[0];
 				lastColorsAdded[0] = color;
 				//and add the brick to the game
 				playfield.add( new Brick( this, getBrickImage(color), new Location(x, y), color ) );
 			}
 		}
	}
	
	public void finish()
	{
		Brick.finish();
		Location.finish();
		super.finish();
	}
	
	public void finishCombo()
	{
		if (comboCount > 1)
		{
			finalComboVal = comboCount;
			drawingCombo = true;
			comboDisplayTimer.refresh();
		}	
		comboCount = 0;
	}
	
	public int getLevel()
	{
		return level;
	}
	
	public void lineBlast(int row)
 	{
 		//add the actual image of the LineBlast explosion
 		playfield.add(new LineBlast(LINE_BLAST, row));
 		
 		/*for (int x = 0; x < 10; x++)
 		{
 			Brick tempBrick = Brick.getPlayingBrickAtLocation(x, row);
 			if (tempBrick != null)
 				bricksInRow.add(tempBrick);
 		}*/
 		ArrayList<Brick> bricksInRow = Brick.getBricksInRow(row);
 		comboCount++;
 		explodeBricks(bricksInRow, 2);
 		bricksExploded = true;
 	}
	 	 	
 	public void loading(long elapsedTime)
 	{
 		if (loadingTimer.action(elapsedTime))
 		{
 			gameState = PLAYING;
 			advancementTimer.refresh();
 		}
 	}
 	
 	public void movePlayer(int direction)
 	{
 		int brickX, brickY;
 		int playerX = player.getLocX();
 		int playerY = player.getLocY();
 		
 		//for every held brick...
 		for (Brick heldBrick : player.getHeldBricks())
 		{
 			brickX = heldBrick.getLocX();
 			brickY = heldBrick.getLocY();
 			
 			//...if the brick can't move (it's adjacent space is occupied), don't move
 			if ( Location.locationOccupied(brickX + direction, brickY) )
 				return;
 		}
 		//...also don't move if the player's side is obstructed by a brick
 		if ( Location.locationOccupied(playerX + direction, playerY) )
 			return;
 		
 		//otherwise, move the player and the selection column
 		player.moveX(direction);
 		column.moveX(direction);
 	}

	public void pullBricks()
	{
		int MAX_BRICKS_HELD = player.MAX_BRICKS_HELD;
		int numHeldBricks = player.getNumHeldBricks();			
		ArrayList<Brick> bricks = Brick.getPlayingBricks();
		Brick lowestBrick;
		
		while (numHeldBricks < MAX_BRICKS_HELD)
		{
			lowestBrick = null;
			for (Brick tempBrick : bricks)
			{
				//if brick is in the player's column and isn't already held
				if (player.compareX(tempBrick) == 0 && tempBrick.getMovementState() != Brick.PULLED) 
				{
					if (lowestBrick == null)
						lowestBrick = tempBrick;
					else if (tempBrick.compareY(lowestBrick) < 0)
						lowestBrick = tempBrick;
				}
			}
			
			//if there is a brick in the column, and the lowest brick is not exploding...
			if (lowestBrick != null && lowestBrick.isExploding() == false)
			{	
				//...and it's not a Stone, Steel, or Skull Brick
				if (lowestBrick.getBrickType() < STONE)
				{
					//if the player isn't holding any bricks, set its color to the chosen brick's color
					if (player.getColor() == COLORLESS)	
						player.setColor( lowestBrick.getColor() );
						
					//if it's the right color brick, pull it
					if (lowestBrick.getColor() == player.getColor())
					{
						player.addHeldBrick(lowestBrick);
						lowestBrick.moveY(new Location(player.getLocX(), player.getLocY() - (1+numHeldBricks)));
						lowestBrick.setMovementState(Brick.PULLED);
						numHeldBricks++;
					}
					//finish if the current brick is the wrong color...
					else return;
					
					//...or if the player just pulled a powerup...
					if (player.getColor() == COLORLESS)
						return;
				}
				//...or if it's a HarmfulBrick...
				else return;
			}
			//...or if there are no more bricks in the column (also finish if lowest brick is exploding)...
			else return;
			
		} //...or if the player has reached MAX_HELD_BRICKS (while loop's condition)
	}
	
	public void radioBlast(Location loc)
	{
		//add the actual image of the RadioBlast explosion
		playfield.add(new RadioBlast( RADIO_BLAST, new Location(loc.getX()-1, loc.getY()-1) ));
		
		//find all bricks that are one space away in either the x or y direction
		ArrayList<Brick> adjacentBricks = new ArrayList<Brick>();
		int locX = loc.getX();
		int locY = loc.getY();
		
		for (int deltaX = -1; deltaX < 2; deltaX++)
		{
			for (int deltaY = -1; deltaY < 2; deltaY++)
			{
				Location tempLoc = new Location(locX + deltaX, locY + deltaY);
				Brick tempBrick = Brick.getPlayingBrickAtLocation(tempLoc);
				if (tempBrick != null)
					adjacentBricks.add(tempBrick);
			}
		}
		comboCount++;
		explodeBricks(adjacentBricks, 2);
		bricksExploded = true;
	}
	
	public void throwBricks()
	{
		int numHeldBricks = player.getNumHeldBricks();
		if (numHeldBricks > 0)
		{
			//Find lowest brick in the player's column	
			ArrayList<Brick> bricks = Brick.getAllBricks();
			Brick lowestBrick = null;
			for (Brick tempBrick : bricks)
			{
				//if brick is in the player's column and isn't already held
				if (player.compareX(tempBrick) == 0 && !tempBrick.isPulled()) 
				{
					if (lowestBrick == null)
						lowestBrick = tempBrick;
					else if (tempBrick.compareY(lowestBrick) < 0)
						lowestBrick = tempBrick;
				}
			}
			Location lowestLoc;
			if (lowestBrick == null)
				lowestLoc = new Location(player.getLocX(), -1);
			else
				lowestLoc = lowestBrick.getLocation();
			//Move held bricks
			{
				int pos = 0; 	//used to determine where each brick is moved to
				Brick tempBrick;
				for (int arrayIndx = player.getNumHeldBricks()-1; arrayIndx >= 0; arrayIndx--)
				{
					tempBrick = player.getHeldBrick(arrayIndx);
					pos++;
					tempBrick.setY(lowestLoc.getY() + pos);
				}
			}
			
			//Return each held Brick's status to not-held by the player
			for (Brick tempBrick : player.getHeldBricks())
				tempBrick.setMovementState(Brick.THROWN);
			
			//Remove each Brick from the heldBricks array	
			player.clearHeldBricks();
			
			//Set player's held-brick-color to COLORLESS
			player.setColor(COLORLESS);
		}
	}
	
	public void updateComboDisplay(long elapsedTime)
	{
 		if (drawingCombo)
 		{ 	
 			//Stop displaying the combo count after a few seconds		
 			if (comboDisplayTimer.action(elapsedTime))
 				drawingCombo = false;
 		}

	}
	
 /****************************************************************************/
 /****************************** RENDER GAME *********************************/
 /****************************************************************************/
 	public void render(Graphics2D g)
 	{
 		switch (gameState)
 		{
 			case LOADING:
 				clrBackground.render(g);
 				loadingFont.drawString(g, "LOADING...", 300, 150);
 				loadingFont.drawString(g, "CONTROLS:", 300, 300);
 				loadingFont.drawString(g, "A, D, Click / J, Right-Click / K", 50, 350);
 				loadingFont.drawString(g, "OR", 365, 450);
 				arrow.render(g);
 				break;
 				
 			case PLAYING:
 				playfield.render(g);
 				drawScore(g);
 				drawCombo(g);
 				break;
 				
 			case WON:
 				clrBackground.render(g);
 				loadingFont.drawString(g, "YOU WIN", 300, 300);
 				break;
 				
 			case LOST:
 				clrBackground.render(g);
 				loadingFont.drawString(g, "GAME OVER", 300, 300);

 		}
 	}
 	
 	public void drawScore(Graphics2D g)
 	{
 	}
 	
 	public void drawCombo(Graphics2D g)
 	{
 		if (drawingCombo)
 			font_40pt.drawString(g, "x"+finalComboVal, 130, 185);
 	}
 	
 /****************************************************************************/
 /***************************** UTILITY METHODS ******************************/
 /****************************************************************************/
	public BufferedImage[] getBrickImage(int colorID)
	{
		switch (colorID)
		{
			case COLORLESS:		return null;
			case RED:			return BRICK_RED;
			case GREEN:			return BRICK_GREEN;
			case BLUE:			return BRICK_BLUE;
			case YELLOW:		return BRICK_YELLOW;
			case PURPLE:		return BRICK_PURPLE;
			case RADIOBLAST:	return BRICK_RADIO;
			case LINEBLAST:		return BRICK_LINEBLAST;
			case COLBLAST:		return BRICK_COLBLAST;
			case STONE:			return BRICK_STONE;
			case STEEL:			return BRICK_STEEL;
			case SKULL:			return BRICK_SKULL;
		}
		return null;
	}
}