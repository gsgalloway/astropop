package com.golden.astropop.sprites.powerups;

//Java
import java.awt.image.BufferedImage;

//Astropop
import com.golden.astropop.Location;
import com.golden.astropop.sprites.powerups.PowerupAnimation;

public class LineBlast extends PowerupAnimation
{	

 /****************************************************************************/
 /******************************* CONSTRUCTOR ********************************/
 /****************************************************************************/
	public LineBlast(BufferedImage[] image, int row)
	{
		//Location loc = new Location(0, row);
		super(image, 200, row*25 + 5);
		getAnimationTimer().setDelay(75);
	}
}