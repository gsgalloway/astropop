package com.golden.astropop.sprites.powerups;

//Java
import java.awt.image.BufferedImage;

//Astropop
import com.golden.astropop.Location;
import com.golden.astropop.sprites.powerups.PowerupAnimation;

public class ColBlast extends PowerupAnimation
{	
 /****************************************************************************/
 /******************************* CONSTRUCTOR ********************************/
 /****************************************************************************/
	public ColBlast(BufferedImage[] image, int column)
	{
		super(image, column*50 + 188, 30);
		getAnimationTimer().setDelay(64);  //45
	}
}