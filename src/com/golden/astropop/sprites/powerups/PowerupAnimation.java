package com.golden.astropop.sprites.powerups;

//Java
import java.awt.image.BufferedImage;
//GTGE
import com.golden.gamedev.object.sprite.VolatileSprite;
//Astropop
import com.golden.astropop.Location;

public class PowerupAnimation extends VolatileSprite
{
	//Non-static instance fields
	Location loc;
	
	//Static instance fields
	private static int numPowerupsAnimating = 0;
	
 /****************************************************************************/
 /******************************* CONSTRUCTOR ********************************/
 /****************************************************************************/
	public PowerupAnimation(BufferedImage[] image, int x, int y)
	{
		super(image, x, y);
		this.loc = loc;
		numPowerupsAnimating++;
	}
	
 /****************************************************************************/
 /******************************* OVERRIDDEN *********************************/
 /****************************************************************************/
 	public void setActive(boolean b)
 	{
 		if (b == false)
 			numPowerupsAnimating--;
 		super.setActive(b);
 	}
 	
 /****************************************************************************/
 /************************* STATIC UTILITY METHODS ***************************/
 /****************************************************************************/
 	public static boolean powerupsAnimating()
 	{
 		if (numPowerupsAnimating > 0)
 			return true;
 			
 		return false;
 	}
}