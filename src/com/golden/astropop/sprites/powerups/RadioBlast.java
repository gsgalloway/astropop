package com.golden.astropop.sprites.powerups;

//Java
import java.awt.image.BufferedImage;

//Astropop
import com.golden.astropop.Location;
import com.golden.astropop.sprites.powerups.PowerupAnimation;

public class RadioBlast extends PowerupAnimation
{	
 /****************************************************************************/
 /******************************* CONSTRUCTOR ********************************/
 /****************************************************************************/
	public RadioBlast(BufferedImage[] image, Location loc)
	{
		super(image, loc.getPixelX(), loc.getPixelY());
		getAnimationTimer().setDelay(25);
	}
}