package com.golden.astropop.sprites;

//Java
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

//GTGE
import com.golden.gamedev.object.Sprite;

public class ProgressBar extends Sprite
{
	private int value;
	private int maxValue;
	
	Graphics2D barImageGraphics;
	
	private Color foregroundColor;
	
	public ProgressBar(BufferedImage image, int x, int y, int width, int height, int maxValue)
	{
		super (image, x, y);
		
		value = 0;
		setMaxValue(maxValue);
		
		// get the barImage's graphics context
		barImageGraphics = (Graphics2D)getImage().getGraphics();
		// set color of progress bar
        barImageGraphics.setColor(Color.ORANGE);
        // the background is transparent
        barImageGraphics.setBackground(new Color(0, 0, 0, 0));
	}
	
	protected void finalize() throws Throwable
	{
		// erase the progress bar's image
		barImageGraphics.clearRect(4, 5, getWidth()-9, getHeight());
		super.finalize();
	}
	
	public void incrementValue()
    {
        // make sure we dont go over the max value
        if (value < maxValue)
            value++;
    }

    public void decrementValue()
    {
        // make sure we dont go below 0
        if (value > 0)
            value--;
    }

    public void setValueToMin()
    {
        value = 0;
    }

    public void setValueToMax()
    {
        value = maxValue;
    }
    
    public void setValue(int value)
    {
        // change value as long as it's not negative and less than maxValue
        if (value >= 0 && value <= maxValue)
            this.value = value;
    }    
    
    public void setMaxValue(int maxValue)
    {
        // change maxValue as long as it's not negative
        if (maxValue >= 0)
            this.maxValue = maxValue;
        
        // correct value if it is over maxValue
        if (value > maxValue)
            value = maxValue;
    }
    
    public void setForegroundColor(Color color)
    {
        foregroundColor = color;
    }
    
    public int getHeight()
    {
    	return super.getHeight() - 10;
    }
    
    public int getValue()
    {
        return value;
    }    
    
    public int getMaxValue()
    {
        return maxValue;
    }    
    
    public double getPercent()
    {
        // make sure you don't divide by zero
        if (value > 0 && maxValue > 0)
            return (double)value / (double)maxValue;
        else
            return 0;
    }
    
    public void update(long elapsedTime)
    {
		int amountFilled = (int)((getHeight()) * getPercent());
		
		// erase current progress bar image (so a decrement in progress is visible)
		//barImageGraphics.clearRect(4, 5, getWidth()-9, getHeight());
		
        // draw the completed portion of the progress bar
        barImageGraphics.fillRect(4, getHeight()-amountFilled + 5, (getWidth() - 9), amountFilled);        
        
        // call Sprite.update()
        super.update(elapsedTime);
    }	
}