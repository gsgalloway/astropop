package com.golden.astropop.sprites;

//Java
import java.awt.image.BufferedImage;
import java.util.ArrayList;
//GTGE
import com.golden.gamedev.object.Sprite;
import com.golden.gamedev.object.Timer;
//Astropop
import com.golden.astropop.Location;

public class Player extends AstropopSprite
{
	public Timer moveTimer = new Timer(125);
	
	private int color;
	public static final int MAX_BRICKS_HELD = 6;
	
	private ArrayList<Brick> heldBricks = new ArrayList<Brick>(6);
	
 /****************************************************************************/
 /******************************* CONSTRUCTOR ********************************/
 /****************************************************************************/
	public Player(BufferedImage image, Location loc)
	{
		super(image, loc);
		getLocation().setType(Location.PLAYER_LOC);
		color = -1; //set as colorless
		Location.addOccupiedLocation(this.getLocation());
	}
	
 /****************************************************************************/
 /****************************** ACCESS METHODS ******************************/
 /****************************************************************************/		
	public int getColor() {
		return color;
	}
	
	public Brick getHeldBrick(int arrayIndex) {
		return heldBricks.get(arrayIndex);
	}
	
	public ArrayList<Brick> getHeldBricks() {
		return heldBricks;
	}
	
	public int getNumHeldBricks() {
		return heldBricks.size();
	}

 /****************************************************************************/
 /***************************** UTILITY METHODS ******************************/
 /****************************************************************************/
	public void addHeldBrick(Brick brick) {
		heldBricks.add(brick);
	}
	
	public void clearHeldBricks() {
		heldBricks.clear();
	}
	
	public void moveX(int direction)
	{
		super.moveX(direction);
		for (Brick heldBrick : heldBricks)
			heldBrick.moveX(direction);
	}
	
	public void setColor(int color) {
		this.color = color;
	}
}