package com.golden.astropop.sprites;

//Java
import java.awt.image.BufferedImage;
import java.util.ArrayList;
//GTGE
import com.golden.gamedev.object.Timer;
//Astropop
import com.golden.astropop.Location;
import com.golden.astropop.Astropop;

public class HarmfulBrick extends Brick 
{	
	private int health;
	private Timer infectionTimer;

	private static ArrayList<HarmfulBrick> harmfulBricks = new ArrayList<HarmfulBrick>();
	
 /****************************************************************************/
 /******************************* CONSTRUCTOR ********************************/
 /****************************************************************************/
	public HarmfulBrick(Astropop game, BufferedImage[] image, Location loc, int color)
	{
		super(game, image, loc, color);
		
		if (color == SKULL)
			infectionTimer = new Timer(4000);
		else
			infectionTimer = new Timer(5000);
			
		if (color == STEEL)
			health = 2;
		else
			health = 1;
			
		harmfulBricks.add(this);
	}
	
 /****************************************************************************/
 /******************************* OVERRIDDEN *********************************/
 /****************************************************************************/
	public void update(long elapsedTime)
	{
		switch (getColor())
		{
			case STONE:
				break;
			case STEEL:
				break;
			case SKULL:
				//if not playing, refresh infection countdown
				if (!isPlaying())
					infectionTimer.refresh();
					
				//otherwise, if infection countdown has elapsed...
				else if (infectionTimer.action(elapsedTime))
				{
					ArrayList<Brick> adjBricks = Brick.getAdjacentBricks(this, 1);
					int randIndx = (int)(Math.random() * adjBricks.size());
					//adjBricks.get(randIndx).infect();
				}
				break;
		}
		super.update(elapsedTime);
	}
	
	public boolean explode() 
	{
		health--;
		if (health == 0)
		{
			setFrame(0);
			setExploding(true);
			return true;
		}
		else
		{
			setFrame(1);
			return false;
		}
	} 
	
 /****************************************************************************/
 /************************* STATIC UTILITY METHODS ***************************/
 /****************************************************************************/
 	public static void finish() {
 		harmfulBricks.clear();
 	}
 
 	/*public static ArrayList<HarmfulBrick> getAdjacentHarmfulBricks(ArrayList<Brick> bricks, int distanceOutward)
 	{
		ArrayList<HarmfulBrick> returnedBricks = new ArrayList<HarmfulBrick>();
		for (Brick tempBrick : bricks)
		{
			Outer:
			for (HarmfulBrick tempHarmBrick : getPlayingHarmfulBricks())
			{
				if (Math.abs(tempHarmBrick.compareX(tempBrick)) == distanceOutward)
				{
					if (tempHarmBrick.compareY(tempBrick) == 0)
					{
						//if brick is already in the returned array
						for (HarmfulBrick tempReturned : returnedBricks)
						{
							if (tempHarmBrick.equals(tempReturned))
								continue Outer;  //go to the next harmful brick
						}
						returnedBricks.add(tempHarmBrick); //otherwise add it to the returned array
					}
				}
				else if (Math.abs(tempHarmBrick.compareY(tempBrick)) == distanceOutward)
				{
					if (tempHarmBrick.compareX(tempBrick) == 0)
					{
						//if brick is already in the returned array
						for (HarmfulBrick tempReturned : returnedBricks)
						{
							if (tempHarmBrick.equals(tempReturned))
								continue Outer;  //go to the next harmful brick
						}
						returnedBricks.add(tempHarmBrick); //otherwise add it to the returned array
					}
				}
			}
		}
		return returnedBricks;
 	}
 
 	public static ArrayList<HarmfulBrick> getPlayingHarmfulBricks() 
 	{
 		ArrayList<HarmfulBrick> returnedBricks = new ArrayList<HarmfulBrick>();
		for (HarmfulBrick tempBrick : harmfulBricks)
		{
			if ( tempBrick.isPlaying() )
				returnedBricks.add(tempBrick);
		}
		return returnedBricks;
 	}
 	
 	public static ArrayList<HarmfulBrick> getPlayingSkullBricks()
 	{
 		ArrayList<HarmfulBrick> returnedBricks = new ArrayList<HarmfulBrick>();
		for (HarmfulBrick tempBrick : harmfulBricks)
		{
			if ( tempBrick.isPlaying() && tempBrick.getColor() == SKULL)
				returnedBricks.add(tempBrick);
		}
		return returnedBricks;
 	}*/
}
