package com.golden.astropop.sprites;

//Java
import java.awt.image.BufferedImage;

//Astropop
import com.golden.astropop.Location;
import com.golden.astropop.sprites.AstropopSprite;

public class ColumnMarker extends AstropopSprite
{	
	public ColumnMarker(BufferedImage image, Location loc)
	{
		super(image, loc);
		getLocation().setType(Location.COLUMN_LOC);
	}
}
