package com.golden.astropop.sprites;

//Java
import java.awt.image.BufferedImage;
import java.util.ArrayList;
//GTGE
import com.golden.gamedev.object.Timer;
//Astropop
import com.golden.astropop.Location;
import com.golden.astropop.Astropop;

public class Brick extends AnimAstropopSprite
{
	//Non-static instance fields
	private boolean movingUp   			= false;
	private boolean hasStartedExploding = false;
	private boolean explodable			= false;
	private boolean infected			= false;
	private boolean exploding 			= false;	
	private int brickType;
	private int generalType;
	private int movementState;
	private int health;
	private Timer conversionTimer; // = new Timer(4000); 	//5000
	private Timer infectionTimer; //  = new Timer(7000);	//7000
	
	//General Brick types
	public static final int COLORED		= 0;
	public static final int POWERUP		= 1;
	public static final int HARMFUL		= 2;

	//Brick movement states
	public static final int NEUTRAL		= 0;
	public static final int PULLED		= 1;
	public static final int THROWN		= 2;
	public static final int ADVANCING 	= 3;
	public static final int FALLING		= 4;
	public static final int EXPLODABLE 	= 5;
	
	//Static instance fields
	private static int level;
	private static int numBricksExploding 	  = 0;
	public  static int numSkullBricks		  = 0;
	private static boolean bricksHaveExploded = false;
	private static boolean bricksHaveFallen   = false;
	private static ArrayList<Brick> allBricks 		= new ArrayList<Brick>(240);
	private static ArrayList<Brick> convertedBricks = new ArrayList<Brick>();
	
	// Brick type variables
	public static final int COLORLESS  	= -1;	//Color types
	public static final int RED 	   	= 0;
	public static final int GREEN  	   	= 1;
	public static final int BLUE 	   	= 2;
	public static final int YELLOW 	   	= 3;
	public static final int PURPLE     	= 4;
	public static final int TURQUOISE  	= 5; 	
		
	public static final int RADIOBLAST 	= 6; 	//Power-ups
	public static final int LINEBLAST  	= 7; 	
	public static final int COLBLAST	= 8;	
	public static final int SUPERCHARGE	= 9;	
	public static final int BONUS		= 10;	
		
	public static final int STONE 	   	= 11;	//HarmfulBrick types
	public static final int STEEL	   	= 12;
	public static final int SKULL	   	= 13;

	
 /****************************************************************************/
 /******************************* CONSTRUCTOR ********************************/
 /****************************************************************************/
	public Brick(Astropop game, BufferedImage[] image, Location loc, int brickType)
	{
		super(game, image, loc);
		
		level = game.getLevel();
		conversionTimer = new Timer(4000 - 250 * level);
		infectionTimer  = new Timer(7000 - 250 * level);
		
		getLocation().setType(Location.BRICK_LOC);
		getAnimationTimer().setDelay(125); //125
		movementState = NEUTRAL;
		setAnimationFrame(0, 2);
		this.brickType = brickType;
		
		switch (brickType)
		{
			case RED:
			case GREEN:
			case BLUE:
			case YELLOW:
			case PURPLE:
			case TURQUOISE:
				health = 1;
				generalType = COLORED;
				break;
				
			case RADIOBLAST:
			case LINEBLAST:
			case COLBLAST:
			case SUPERCHARGE:
			case BONUS:
				health = 1;
				generalType = POWERUP;
				break;
					
			case STONE:
				health = 1;
				generalType = HARMFUL;
				break;	
			case STEEL:
				health = 2;
				generalType = HARMFUL;
				break;
			case SKULL:
				health = 1;
				generalType = HARMFUL;
				infected = true;

		}			
		allBricks.add(this);
		Location.addOccupiedLocation(this.getLocation());
	}
	
 /****************************************************************************/
 /******************************* OVERRIDDEN *********************************/
 /****************************************************************************/
	public void update(long elapsedTime)
	{
		if (isExplodable())
			explodable = false;
		
		//Handle events that trigger when bricks move in any way	
		if (movementState == PULLED)
			setInfected(false);  //remove infection from pulled bricks
		else if (movementState == THROWN) 
		{
			//The COLBLAST powerup brick has its effect triggered once it's thrown, too
			if (brickType == COLBLAST)
			{
				movementState = NEUTRAL;
				explode();
			}
		}
		if (movementState != NEUTRAL)  //If the brick's moving in any way...
		{
			//when done moving into place (moveTo() returns true) -- make brick eligible for explosion
			if ( moveTo(elapsedTime, getPixelX(), getLocation().getPixelY(), 3) )
			{	
				if (isThrown())
				{
					if (generalType == POWERUP)
					{
						movementState = NEUTRAL;
						explode();
					}
					else
					{
						movementState = NEUTRAL;
						if (getColor() != COLORLESS)  //don't blow up harmful bricks
						{	
							explodable = true;
							bricksHaveFallen = true;
						}
					}
				}
				else if (isFalling())
				{
					movementState = NEUTRAL;
					if (getColor() != COLORLESS)
					{	
						explodable = true;
						bricksHaveFallen = true;
					}
				}		
			}
		}
		
		/*** Inefficient version of the above that I'm afraid to throw away just yet... ***/
		/*switch (movementState)
		{
			case NEUTRAL: 
				break;
				
			case PULLED: 
				setInfected(false);
			case THROWN:
				
			case ADVANCING:
			case FALLING:
				//when done moving into place (moveTo() returns true), make brick eligible for explosion
				if ( moveTo(elapsedTime, getPixelX(), getLocation().getPixelY(), 3) )
				{	
					if (isThrown())
					{
						if (generalType == POWERUP)
						{
							movementState = NEUTRAL;
							explode();
						}
						else
						{
							movementState = NEUTRAL;
							if (getColor() != COLORLESS)
							{	
								explodable = true;
								bricksHaveFallen = true;
							}
						}
					}
					else if (isFalling())
					{
						movementState = NEUTRAL;
						if (getColor() != COLORLESS)
						{	
							explodable = true;
							bricksHaveFallen = true;
						}
					}		
				}
		}*/
		if (exploding)
		{
				if (!hasStartedExploding)
				{
					setAnimate(true);
					hasStartedExploding = true;
					numBricksExploding++;
				}
				else if (!isAnimate())
				{
					removeBrick(this);
					bricksHaveExploded = true;
					numBricksExploding--;
				}
		}
		else if (brickType == SKULL && numSkullBricks < 8)
		{
			//if not playing, refresh infection countdown
			if (!isPlaying())
				infectionTimer.refresh();
				
			//otherwise, if infection countdown has elapsed...
			else if (infectionTimer.action(elapsedTime))
			{
				ArrayList<Brick> adjBricks = Brick.getAdjacentBricks(this, 1);
				int numAdjBricks = adjBricks.size();
				int randIndx;
				Brick chosenBrick;
				if (numAdjBricks != 0)
				{
					for (int x = 0; x <= numAdjBricks; x++)
					{
						randIndx = ((int)(Math.random() * numAdjBricks + x)) % (numAdjBricks);
						chosenBrick = adjBricks.get(randIndx);
						if (chosenBrick.isInfected() == false && chosenBrick.isExploding() == false)
						{
							chosenBrick.setInfected(true);
							numSkullBricks++;
							break;
						}
					}
				}	
			}
		}
		else if (infected)
		{
			if (conversionTimer.action(elapsedTime))
				convertedBricks.add(this);
		}
		super.update(elapsedTime);
	}
	
 /****************************************************************************/
 /****************************** ACCESS METHODS ******************************/
 /****************************************************************************/	
	public int getBrickType() {
		return brickType;
	}
	
	public int getColor()
	{
		if (brickType <= TURQUOISE)
			return brickType;
		else 
			return COLORLESS;
	}
	
	public int getMovementState() {
		return movementState;
	}
	
	public boolean isAdvancing() {
		return (movementState == ADVANCING);
	}
	
	public boolean isExplodable() {
		return explodable;
	}
	
	public boolean isExploding() {
		return exploding;
	}
	
	public boolean isFalling() {
		return (movementState == FALLING);
	}
	
	public boolean isHarmful() {
		return generalType == HARMFUL;
	}
	
	public boolean isInfected() {
		return infected;
	}
	
	public boolean isPlaying() {
		return (getLocY() > 0);
	}
	
	public boolean isPowerup() {
		return generalType == POWERUP;
	}
	
	public boolean isPulled() {
		return (movementState == PULLED);
	}
	
	public boolean isThrown() {
		return (movementState == THROWN);
	}
	
	public boolean isMoving() {
		return (isPulled() || isThrown() || isAdvancing() || isFalling());
	}

 /****************************************************************************/
 /***************************** UTILITY METHODS ******************************/
 /****************************************************************************/
 	public boolean equals(Brick brick2) {
		return ( getLocation().equals(brick2.getLocation()) );
	}
	
	public boolean explode() {
		//don't do any of the below if the brick is already exploding
		if (isExploding())
			return false;
			
		health--;
		//if it was not a steel brick (one point of damage was enough to break it...)
		if (health == 0)
		{
			setInfected(false);
			setFrame(0);
			setAnimationFrame(0, 2);
			setExploding(true);	
			
			//if it's a powerup, tell the game to perform powerup brick ability
			if (generalType == POWERUP)
			{
				switch (brickType)
				{
					case RADIOBLAST:
						getGame().radioBlast(getLocation());
						break;
					case LINEBLAST:
						getGame().lineBlast(getLocY());
						break;
					case COLBLAST:
						getGame().colBlast(getLocX());
						break;
					case SUPERCHARGE:
						break;
					case BONUS:
						break;
				}
			}
		}
		//if it was a steel brick, set its animation frame to the partially damaged image
		else
			setFrame(1);
		
		return true;
	} 
		
	public void explode(int damage) {
		//don't do any of the below if the brick is already exploding
		if (isExploding())
			return;
			
		health -= damage;
		if (health < 1)
		{
			setInfected(false);
			setFrame(0);
			setAnimationFrame(0, 2);
			setExploding(true);	
				
			//if it's a powerup, tell the game to perform powerup brick ability
			if (generalType == POWERUP)
			{
				switch (brickType)
				{
					case RADIOBLAST:
						getGame().radioBlast(getLocation());
						break;
					case LINEBLAST:
						getGame().lineBlast(getLocY());
						break;
					case COLBLAST:
						getGame().colBlast(getLocX());
						break;
					case SUPERCHARGE:
						break;
					case BONUS:
						break;
				}
			}
		}
	}
	
	public void setExploding(boolean b) {
		exploding = b;
	}
	
	public void setInfected(boolean b)
	{
		if (b == true)
		{	
			if (!infected)
			{
				infected = true;
				generalType = HARMFUL;
				setAnimationFrame(3, 20);
				setLoopAnim(true);
				setAnimate(true);
				conversionTimer.refresh();
			}
		}
		else
		{
			if (infected)
			{
				Brick.numSkullBricks--;
				infected = false;
				generalType = COLORED;
				setAnimate(false);
				setLoopAnim(false);
				setAnimationFrame(0, 2);
				setFrame(0);
				getAnimationTimer().refresh();
			}
		}
	}
	
	public void setMovementState(int movementState) {
		this.movementState = movementState;
	}
	
	public String toString() {
		return (getLocation().toString() + " BrickType: " + brickType);
	}
	
 /****************************************************************************/
 /************************* STATIC UTILITY METHODS ***************************/
 /****************************************************************************/
 	public static boolean bricksAreExploding() {
 		return (numBricksExploding > 0);
 	}
 
 	public static boolean bricksHaveExploded() {
 		return bricksHaveExploded;
 	}
 	
 	public static boolean bricksHaveFallen() {
 		return bricksHaveFallen;
 	}
 	
 	public static void clearConvertedBricks() {
 		convertedBricks.clear();
 	}
 	
 	public static void finish() 
 	{
 		//clear all static instance fields when the game ends
 		numBricksExploding	= 0;
 		numSkullBricks		= 0;
 		bricksHaveExploded	= false;
 		bricksHaveFallen	= false;
 		convertedBricks.clear();
 		allBricks.clear();
 	}
 
 	public static ArrayList<Brick> getAdjacentBricks(Brick brick, int distanceOutward) 
 	{
		ArrayList<Brick> playingBricks = getPlayingBricks();
		ArrayList<Brick> returnedBricks = new ArrayList<Brick>(4);
		for (Brick tempBrick : playingBricks)
		{
			if (Math.abs(tempBrick.compareX(brick)) == distanceOutward)
			{
				if (tempBrick.compareY(brick) == 0)
					returnedBricks.add(tempBrick);
			}
			else if (Math.abs(tempBrick.compareY(brick)) == distanceOutward)
			{
				if (tempBrick.compareX(brick) == 0)
					returnedBricks.add(tempBrick);
			}
		}
		return returnedBricks;
	}
	
	public static ArrayList<Brick> getAdjacentHarmfulBricks(ArrayList<Brick> bricks, int distanceOutward)
 	{
		ArrayList<Brick> returnedBricks = new ArrayList<Brick>();
		for (Brick tempBrick : bricks)
		{
			HarmfulBrickLoop:
			for (Brick tempHarmBrick : getPlayingHarmfulBricks())
			{
				if (Math.abs(tempHarmBrick.compareX(tempBrick)) == distanceOutward)
				{
					if (tempHarmBrick.compareY(tempBrick) == 0)
					{
						//if brick is already in the returned array...
						for (Brick tempReturned : returnedBricks)
						{
							if (tempHarmBrick.equals(tempReturned))
								continue HarmfulBrickLoop;  //...go to the next harmful brick
						}
						//...also skip this brick if it's already in the list of bricks to be exploded
						for (Brick tempBrick2 : bricks)
						{
							if (tempHarmBrick.equals(tempBrick2))
								continue HarmfulBrickLoop;
						}
						returnedBricks.add(tempHarmBrick); //otherwise add it to the returned array
					}
				}
				else if (Math.abs(tempHarmBrick.compareY(tempBrick)) == distanceOutward)
				{
					if (tempHarmBrick.compareX(tempBrick) == 0)
					{
						//if brick is already in the returned array
						for (Brick tempReturned : returnedBricks)
						{
							if (tempHarmBrick.equals(tempReturned))
								continue HarmfulBrickLoop;  //go to the next harmful brick
						}
						returnedBricks.add(tempHarmBrick); //otherwise add it to the returned array
					}
				}
			}
		}
		return returnedBricks;
 	}
 	
 	public static ArrayList<Brick> getAdjacentPowerups(ArrayList<Brick> bricks, int distanceOutward)
 	{
		ArrayList<Brick> returnedBricks = new ArrayList<Brick>();
		for (Brick tempBrick : bricks)
		{
			PowerupLoop:
			for (Brick tempPowerup : getPlayingPowerups())
			{
				if (Math.abs(tempPowerup.compareX(tempBrick)) == distanceOutward)
				{
					if (tempPowerup.compareY(tempBrick) == 0)
					{
						//if brick is already in the returned array...
						for (Brick tempReturned : returnedBricks)
						{
							if (tempPowerup.equals(tempReturned))
								continue PowerupLoop;  //...go to the next harmful brick
						}
						//...also skip this brick if it's already in the list of bricks to be exploded
						for (Brick tempBrick2 : bricks)
						{
							if (tempPowerup.equals(tempBrick2))
								continue PowerupLoop;
						}
						returnedBricks.add(tempPowerup); //otherwise add it to the returned array
					}
				}
				else if (Math.abs(tempPowerup.compareY(tempBrick)) == distanceOutward)
				{
					if (tempPowerup.compareX(tempBrick) == 0)
					{
						//if brick is already in the returned array
						for (Brick tempReturned : returnedBricks)
						{
							if (tempPowerup.equals(tempReturned))
								continue PowerupLoop;  //go to the next harmful brick
						}
						returnedBricks.add(tempPowerup); //otherwise add it to the returned array
					}
				}
			}
		}
		return returnedBricks;
 	}	
	
	public static ArrayList<Brick> getAdjacentSameColorBricks(Brick testBrick, ArrayList<Brick> totalAdjacentClrdBrcks)
	{
		ArrayList<Brick> returnedBricks = totalAdjacentClrdBrcks;
		
		//For each adjacent brick
		Outer:
		for (Brick tempAdjacentBrick : getAdjacentBricks(testBrick, 1))
		{
			//if it's the same color
			if (tempAdjacentBrick.getColor() == testBrick.getColor())
			{
				//if the brick is already in the array...
				for (Brick tempReturnedBrick : returnedBricks)
				{
					if ( tempAdjacentBrick.equals(tempReturnedBrick) )
						continue Outer; //... go to the next brick
				}
				//otherwise, add it to the array
				returnedBricks.add(tempAdjacentBrick);
				//then recursively add bricks adjacent to this one
				returnedBricks = ( getAdjacentSameColorBricks(tempAdjacentBrick, returnedBricks) );
			}
		}
		return returnedBricks;
	}

	public static ArrayList<Brick> getAllBricks() {
		return allBricks;
	}
		
	public static Brick getBrickAtLocation(Location loc)
	{
		for (Brick testBrick : allBricks)
		{
			if ( testBrick.isAtLocation(loc) )
				return testBrick;
		}
		return null;
	}
	
	public static Brick getBrickAtLocation(int locX, int locY)
	{
		for (Brick testBrick : allBricks)
		{
			if ( testBrick.isAtLocation(new Location(locX, locY)) )
				return testBrick;
		}
		return null;
	}
	
	public static ArrayList<Brick> getBricksInColumn(int column)
	{
		ArrayList<Brick> returnedBricks = new ArrayList<Brick>();
		Brick tempBrick;
		for (int x = 1; x < 22; x++)
		{
			tempBrick = getPlayingBrickAtLocation(column, x);
			if (tempBrick != null)
				returnedBricks.add(tempBrick);
		}
		return returnedBricks;
	}
	
	public static ArrayList<Brick> getBricksInRow(int row)
	{
		ArrayList<Brick> returnedBricks = new ArrayList<Brick>();
		Brick tempBrick;
		for (int x = 0; x < 10; x++)
 		{
 			tempBrick = getPlayingBrickAtLocation(x, row);
 			if (tempBrick != null)
 				returnedBricks.add(tempBrick);
 		}
 		return returnedBricks;
	}
	
	public static ArrayList<Brick> getConvertedBricks() {
		return convertedBricks;
	}

	public static Brick getPlayingBrickAtLocation(Location loc)
	{
		for (Brick testBrick : getPlayingBricks())
		{
			if ( testBrick.isAtLocation(loc) )
				return testBrick;
		}
		return null;
	}
	
	public static Brick getPlayingBrickAtLocation(int locX, int locY)
	{
		for (Brick testBrick : getPlayingBricks())
		{
			if ( testBrick.isAtLocation(new Location(locX, locY)) )
				return testBrick;
		}
		return null;
	}
	
	public static ArrayList<Brick> getPlayingBricks() 
	{
		ArrayList<Brick> returnedBricks = new ArrayList<Brick>();
		for (Brick tempBrick : allBricks)
		{
			if (tempBrick.isPlaying() && !tempBrick.isPulled())
				returnedBricks.add(tempBrick);
		}
		return returnedBricks;
	}

 	public static ArrayList<Brick> getPlayingHarmfulBricks() 
 	{
 		ArrayList<Brick> returnedBricks = new ArrayList<Brick>();
		for (Brick tempBrick : allBricks)
		{
			if ( tempBrick.isHarmful() && tempBrick.isPlaying() )
				returnedBricks.add(tempBrick);
		}
		return returnedBricks;
 	}
 	
 	public static ArrayList<Brick> getPlayingPowerups()
 	{
 		ArrayList<Brick> returnedBricks = new ArrayList<Brick>();
		for (Brick tempBrick : allBricks)
		{
			if ( tempBrick.isPowerup() && tempBrick.isPlaying() )
				returnedBricks.add(tempBrick);
		}
		return returnedBricks;
 	}
 	
 	public static ArrayList<Brick> getPlayingSkullBricks()
 	{
 		ArrayList<Brick> returnedBricks = new ArrayList<Brick>();
		for (Brick tempBrick : allBricks)
		{
			if ( tempBrick.getBrickType() == SKULL && tempBrick.isPlaying())
				returnedBricks.add(tempBrick);
		}
		return returnedBricks;
 	}
	
	public static void setBricksHaveExploded(boolean b) {
		bricksHaveExploded = b;
	}

	public static void setBricksHaveFallen(boolean b) {
		bricksHaveFallen = b;
	}
	
	public static void removeBrick(Brick brick)
	{
		int pos = 0;
		for (Brick tempBrick : allBricks)
		{
			if (brick.equals(tempBrick))
			{
				tempBrick.setActive(false);
				allBricks.remove(pos);
				return;
			}
			pos++;
		}	
	}
}
