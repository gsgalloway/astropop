package com.golden.astropop.sprites;

//Java
import java.awt.image.BufferedImage;
//GTGE
import com.golden.gamedev.object.Sprite;
//Astropop
import com.golden.astropop.Location;

public abstract class AstropopSprite extends Sprite
{
	private Location loc;
	
 /****************************************************************************/
 /******************************* CONSTRUCTOR ********************************/
 /****************************************************************************/
	public AstropopSprite(BufferedImage image, Location loc)
	{
		super(image, loc.getPixelX(), loc.getPixelY());
		this.loc = loc;
	}

 /****************************************************************************/
 /******************************* OVERRIDDEN *********************************/
 /****************************************************************************/	
	public void setActive(boolean active)
	{
		if (!active)
		{
			Location.removeLocation(getLocation());
		}
		super.setActive(active);
	}
	
 /****************************************************************************/
 /****************************** ACCESS METHODS ******************************/
 /****************************************************************************/	
 	public Location getLocation() {
		return loc;
	}
	
	public int getLocX() {
		return loc.getX();
	}
	
	public int getLocY() {
		return loc.getY();
	}
	
	public double getPixelX() {
		return super.getX();
	}
	
	public double getPixelY() {
		return super.getY();
	}
	
 /****************************************************************************/
 /***************************** UTILITY METHODS ******************************/
 /****************************************************************************/
	public int compareX(AstropopSprite sprite) {
		return loc.compareX(sprite.getLocation());
	}
	
	public int compareX(AnimAstropopSprite sprite) {
		return loc.compareX(sprite.getLocation());
	}	
		
	public int compareY(AstropopSprite sprite) {
		return loc.compareY(sprite.getLocation());
	}
	
	public int compareY(AnimAstropopSprite sprite) {
		return loc.compareY(sprite.getLocation());
	}
	
	public boolean isAtLocation(Location loc) {
		return this.loc.equals(loc);
	}
	
	public void moveX(int direction)
	{
		if (getLocX() + direction > -1 && getLocX() + direction < 10) //if movement is within bounds
		{
			loc.moveX(direction);		  //adjust Location
			super.moveX(direction * 50);  //adjust actual position on screen
		}
	}
	
	public void moveY(int amt)
	{
		loc.moveY(amt);
	}
	
	public void moveY(Location newLoc)
	{
		loc.moveY(newLoc); 	//adjust Location
		//adjusting actual position on screen handled by update() method of subclasses
	}
	
	public void setY(int y) {
		loc.setY(y);
	}
}