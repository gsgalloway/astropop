package com.golden.astropop;

import java.util.ArrayList;

public class Location 
{
	private int x, y, type;
	
	private static ArrayList<Location> occupiedLocations = new ArrayList<Location>(240);
	
	//Type flags - designate type of object using this Location
	public static final int BRICK_LOC  = 0;
	public static final int PLAYER_LOC = 1;
	public static final int COLUMN_LOC = 2;

 /****************************************************************************/
 /******************************* CONSTRUCTOR ********************************/
 /****************************************************************************/
    public Location(int x, int y) 
    {
    	this.x = x;
    	this.y = y;
    }
 
 /****************************************************************************/
 /****************************** ACCESS METHODS ******************************/
 /****************************************************************************/	
    public int getType() {
    	return type;
    }
    
    public int getPixelX() {	
    	return x*50 + 200; 
    }
    
    public int getPixelY() {	
    	return (y > 0 ? (y*25 + 5) : 0); 
    }
    
    public int getX() {
    	return x;
    }
    
    public int getY() {
    	return y;
	} 
   
    public String toString() {
    	return "(" + x + ", " + y + ")" ;
    }
    
 /****************************************************************************/
 /***************************** UTILITY METHODS ******************************/
 /****************************************************************************/ 
    public int compareX(Location loc2) {
    	return this.x - loc2.x;
    }
    
    public int compareY(Location loc2) {
    	return loc2.y - this.y;
    }    
    
    public boolean equals(Location loc2) {
    	return ( compareX(loc2) == 0 && compareY(loc2) == 0 );
    }
    
    public void moveX(int amt) 
    {
    	if (type != COLUMN_LOC)
    	{
    		removeLocation(this);
    		x += amt;
    		addOccupiedLocation(this);
    	}
    	else
    		x += amt;
    }
    
    public void moveX(Location loc) {
    	removeLocation(this);
    	x = loc.x;
    	addOccupiedLocation(this);
    }
    
    public void moveY(int amt) {
    	removeLocation(this);
    	y += amt;
    	addOccupiedLocation(this);
    }
    
    public void moveY(Location loc) {
    	removeLocation(this);
    	y = loc.getY();
    	addOccupiedLocation(this);
    }
    
    public void setX(int amt) {
    	removeLocation(this);
    	x = amt;
    	addOccupiedLocation(this);
    }
    
    public void setY(int amt) {
    	removeLocation(this);
    	y = amt;
    	addOccupiedLocation(this);
    }
    	
	public void setType(int flag) {
		type = flag;
	}  
    
 /****************************************************************************/
 /************************* STATIC UTILITY METHODS ***************************/
 /****************************************************************************/
 	public static void addOccupiedLocation(Location loc) {
 		occupiedLocations.add(loc);
 	}
 	
 	public static void finish() {
 		occupiedLocations.clear();
 	}
 	
 	public static boolean locationOccupied(int x, int y) {
 		return (getLocationAt(x, y) != null);
 	}
 	
 	public static boolean locationOccupied(Location loc) {
 		return (getLocationAt(loc) != null);
 	}

 	public static Location getLocationAt(int x, int y)
 	{
 		Location loc = new Location(x, y);
 		for (Location testLoc : occupiedLocations)
 		{
 			if ( testLoc.equals(loc) )
 				return testLoc;
 		}
 		return null;
 	}
 	
 	public static Location getLocationAt(Location loc)
 	{
 		for (Location testLoc : occupiedLocations)
 		{
 			if ( testLoc.equals(loc) )
 				return testLoc;
 		}
 		return null;
 	}
 
 	public static ArrayList<Location> getAdjacentOccupiedLocs(Location loc)
	{
		ArrayList<Location> returnedLocs = new ArrayList<Location>(4);
		for (Location tempLoc : occupiedLocations)
		{
			if ( loc.compareX(tempLoc) == 1 || loc.compareX(tempLoc) == -1 )
			{
				if ( loc.compareY(tempLoc) == 1 || loc.compareY(tempLoc) == -1 )
					returnedLocs.add(tempLoc);
			}
		}
		return returnedLocs;
	}
	
	public static ArrayList<Location> getOccupiedLocs() {
		return occupiedLocations;
	}
	
    public static void removeLocation(Location loc) {
    	int arrayPos = 0;
    	for (Location tempLoc : occupiedLocations)
    	{
    		if (loc.equals(tempLoc))
    		{
    			occupiedLocations.remove(arrayPos);
    			return;
    		}
    		
    		arrayPos++;
    	}
    }
}