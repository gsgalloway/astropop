package com.golden.astropop;

//Java
import java.awt.Dimension;

//GTGE
import com.golden.gamedev.GameEngine;
import com.golden.gamedev.GameObject;
import com.golden.gamedev.GameLoader;

//Astropop
import com.golden.astropop.menu.MainMenu;

public class AstropopGame extends GameEngine
{
	public static final int MAIN_MENU 	= 0;
	public static final int ASTROPOP	= 1;
	
	public int level = 0;
	
 /****************************************************************************/
 /************************* INIT COMMON RESOURCES ****************************/
 /****************************************************************************/
	public void initResources()
	{
		
	}
	
	public GameObject getGame(int gameID)
	{
		switch (gameID)
		{
			case MAIN_MENU:	return new MainMenu(this);
			case ASTROPOP:  return new Astropop(this, ++level);
		}
		
		return null;
	}
	
 /****************************************************************************/
 /****************************** MAIN-CLASS **********************************/
 /****************************************************************************/
 	public static void main(String args[])
 	{
 		GameLoader game = new GameLoader();
 		game.setup(new AstropopGame(), new Dimension(800, 600), false, true); //800, 600
 		game.start();
 	}
 	
 	{ distribute = !true; }
}