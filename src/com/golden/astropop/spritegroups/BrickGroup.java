package com.golden.astropop.spritegroups;

//GTGE
import com.golden.gamedev.object.SpriteGroup;

//Astropop
import com.golden.astropop.Astropop;

public class BrickGroup extends SpriteGroup
{
	public Astropop game;
	
	public BrickGroup(String name, Astropop game)
	{
		super(name);
		this.game = game;
		setComparator(game);
	}
}