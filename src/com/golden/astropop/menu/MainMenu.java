package com.golden.astropop.menu;

//Java
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.*;

//GTGE
import com.golden.gamedev.GameEngine;
import com.golden.gamedev.GameObject;
import com.golden.gamedev.object.*;
import com.golden.gamedev.object.background.ImageBackground;
import com.golden.gamedev.object.font.SystemFont;
import com.golden.gamedev.util.ImageUtil;

//Astropop
import com.golden.astropop.AstropopGame;

public class MainMenu extends GameObject
{
	public AstropopGame game;
	
	PlayField playfield;
	Background background;
	
	public BufferedImage	COLUMN_IMAGE, SHIP_IMAGE;
	
	public GameFont font_40pt;
	public GameFont font;
	
	public Sprite column, ship;
	
	public int option = 0;
	
	public MainMenu(AstropopGame parent)
	{
		super(parent);
		game = parent;
		hideCursor();
	}
	
 /****************************************************************************/
 /**************************** INIT RESOURCES ********************************/
 /****************************************************************************/
	public void initResources()
	{
		background = new ImageBackground(getImage("Assets//Images//Background_Stars.png"));
		playfield = new PlayField(background);
		
		//Load Images
		COLUMN_IMAGE = ImageUtil.getImage(bsIO.getURL("Assets//Images//Sideways_Column.png"),Transparency.TRANSLUCENT);
		SHIP_IMAGE	 = ImageUtil.rotate( getImage ("Assets//Images//Ship.png"), 90 );
		
		//Load Font
		font = fontManager.getFont(getImages("Assets//Images//font.png", 16, 6));
		try
		{
			File file = new File("com//golden//astropop//Assets//Fonts//evanescent.ttf");
			FileInputStream fis = new FileInputStream(file);
			Font tempFont = Font.createFont(Font.TRUETYPE_FONT, fis);
			Font tempFont40 = new Font(tempFont.getFontName(), Font.PLAIN, 52);
			font_40pt = new SystemFont(tempFont40, new Color(234, 188, 29)); //177, 3, 3
		}
		catch (Exception e){System.out.println(e);}
		
		//Astropop Icon
		playfield.add( new Sprite(getImage("Assets//Images//Astropop_Icon.jpg"), 0, 0) );
		
		//Selection column
		column = new Sprite(COLUMN_IMAGE, 0, 250);
 		playfield.add(column);
 		
 		//Ship Image
 		ship = new Sprite(SHIP_IMAGE, 0, 250);
 		playfield.add(ship);
	}
	
 /****************************************************************************/
 /***************************** UPDATE GAME **********************************/
 /****************************************************************************/
 	public void update(long elapsedTime)
 	{
 		checkInput(elapsedTime);
 		column.setY(250 + 50 * option);
 		ship.setY(250 + 50 * option);
 	}
 	
 	public void checkInput(long elapsedTime)
 	{
 		if ( keyPressed(KeyEvent.VK_DOWN) )
 		{
 			if (++option > 2)
 				option = 0;
 		}
 		if ( keyPressed(KeyEvent.VK_UP) )
 		{
 			if (--option < 0)
 				option = 2;
 		}
 		if ( keyPressed(KeyEvent.VK_ENTER) )
 		{
 			switch (option)
 			{
 				case 0: 
 					parent.nextGameID = AstropopGame.ASTROPOP;
					finish();
				case 1:
					finish();
 			}
 		}
 	}
	
 /****************************************************************************/
 /****************************** RENDER GAME *********************************/
 /****************************************************************************/
	public void render(Graphics2D g)
	{
		playfield.render(g);
		font.drawString(g, "Start New Game", 400, 260);
		font.drawString(g, "Instructions",	 400, 310);
		font.drawString(g, "Options",		 400, 360);
		column.render(g);
	}
}